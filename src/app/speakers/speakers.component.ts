import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Speaker} from '../model/speaker';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SpeakerService} from '../services/speakers/speaker.service';
import {Session} from '../model/session';

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.css']
})
export class SpeakersComponent implements OnInit {
  @Output()
  public speakerCreated: EventEmitter<void> | undefined;

  speakers: Speaker[] = [];

  public speakerForm: FormGroup;

  constructor(private speakerService: SpeakerService) {
    this.speakerForm = new FormGroup({
      first_name: new FormControl('', [Validators.required, Validators.min(0)]),
      last_name: new FormControl('', [Validators.required, Validators.min(0)]),
     });
  }

  ngOnInit(): void {
    this.getSpeakers();
  }

  getSpeakers(): void {
    this.speakerService.getSpeakers()
      .toPromise().then(speakers => this.speakers = speakers);
  }

  createSpeaker(): void {
    if (this.speakerForm.valid) {
      const firstNameField = this.speakerForm.get('first_name');
      const lastNameField = this.speakerForm.get('last_name');
      if (firstNameField && lastNameField) {
        this.speakerService.createSpeaker({
          first_name: firstNameField.value,
          last_name: lastNameField.value
        }).toPromise().then(() => {
          this.speakerForm.reset();
          this.getSpeakers();
        });
      }
    }
  }

  delete(speaker: Speaker): void {
    this.speakers = this.speakers.filter(s => s !== speaker);
    this.speakerService.deleteSpeaker(speaker.id).subscribe();
  }
}
