import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Session } from '../model/session';
import { SessionService } from '../services/sessions/session.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.css']
})

export class SessionsComponent implements OnInit {
  @Output()
  public sessionCreated: EventEmitter<void> | undefined;

  sessions: Session[] = [];
  public sessionForm: FormGroup;
  public show: boolean;

  constructor(private sessionService: SessionService) {
    this.sessionForm = new FormGroup({
      session_name: new FormControl('', [Validators.required, Validators.min(0)]),
      session_description: new FormControl('', [Validators.required, Validators.min(0)]),
      session_length: new FormControl('', [Validators.required, Validators.min(0)]),
      session_start: new FormControl('', [Validators.required, Validators.min(0)]),
      session_end: new FormControl('', [Validators.required, Validators.min(0)])
    });
    this.show = false;
  }

  ngOnInit(): void {
    this.getSessions();

  }

  public showCreate(): void {
      this.show = true;
  }

  getSessions(): void {
    this.sessionService.getSessions()
      .toPromise().then(sessions => this.sessions = sessions);
  }

  createSession(): void {
    if (this.sessionForm.valid) {
      const nameField = this.sessionForm.get('session_name');
      const descriptionField = this.sessionForm.get('session_description');
      const lengthField = this.sessionForm.get('session_length');
      const startField = this.sessionForm.get('session_start');
      const endField = this.sessionForm.get('session_end');
      if (nameField && descriptionField && lengthField && startField && endField) {
        this.sessionService.createSession({
          session_name: nameField.value,
          session_description: descriptionField.value,
          session_length: lengthField.value,
          session_start: startField.value,
          session_end: endField.value
        }).toPromise().then(() => {
          this.sessionForm.reset();
          this.getSessions();
          this.show = false;
        });
      }
    }
  }

  delete(session: Session): void {
    this.sessions = this.sessions.filter(s => s !== session);
    this.sessionService.deleteSession(session.id).subscribe();
  }


  public resetState(): void {
    this.show = false;
    this.sessionForm.reset();
  }
}
