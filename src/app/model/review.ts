export interface Review {
  id?: number;
  review_name: string;
  review_description: string;
  score: number;
}
