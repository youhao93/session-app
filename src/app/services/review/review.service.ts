import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MessageService} from '../messages/message.service';
import {Observable, of} from 'rxjs';
import { Review } from '../../model/review';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private reviewUrl = 'http://localhost:8082/api/v1/reviews';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
  }

  /** Log a ReviewService message with the MessageService */
  // tslint:disable-next-line:typedef
  private log(message: string) {
    this.messageService.add(`ReviewService: ${message}`);
  }

  getReviews(): Observable<Review[]> {
    return this.http.get<Review[]>(this.reviewUrl)
      .pipe(
        tap(_ => this.log('fetched reviews')),
        catchError(this.handleError<Review[]>('getReviews', []))
      );
  }

  getReview(id: number): Observable<Review> {
    const url = `${this.reviewUrl}/${id}`;
    return this.http.get<Review>(url).pipe(
      tap(_ => this.log(`fetched review id=${id}`)),
      catchError(this.handleError<Review>(`getReview id=${id}`))
    );
  }

  // PUT request for review on the server
  updateReview(review: Review | undefined): Observable<any> {
    // @ts-ignore
    const id = review.id;
    return this.http.put(this.reviewUrl + `/${id}`, review, this.httpOptions).pipe(
      // @ts-ignore
      tap(_ => this.log(`updated review id=${review.id}`)),
      catchError(this.handleError<any>('updateReview'))
    );
  }


  /** POST: add a new review to the server */
  createReview(review: Review): Observable<Review> {
    return this.http.post<Review>(this.reviewUrl, review, this.httpOptions).pipe(
      tap((newReview: Review) => this.log(`added review w/ id=${newReview.id}`)),
      catchError(this.handleError<Review>('addReview'))
    );
  }


  public deleteReview(id: number | undefined): Observable<Review> {
    return this.http.delete<Review>(this.reviewUrl + '/' + id, this.httpOptions).pipe(
      tap(_ => this.log(`deleted review id=${id}`)),
      catchError(this.handleError<Review>('deleteReview'))
    );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  // tslint:disable-next-line:typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
