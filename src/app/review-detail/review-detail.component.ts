import {Component, Input, OnInit} from '@angular/core';
import { Review } from '../model/review';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ReviewService} from '../services/review/review.service';

@Component({
  selector: 'app-review-detail',
  templateUrl: './review-detail.component.html',
  styleUrls: ['./review-detail.component.css']
})
export class ReviewDetailComponent implements OnInit {

  @Input() review?: Review;

  public reviewForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private reviewService: ReviewService,
    private router: Router
  ) {
    this.reviewForm = new FormGroup( {
      review_name: new FormControl('', [Validators.required, Validators.min(0)]),
      review_description: new FormControl('', [Validators.required, Validators.min(0)]),
      score: new FormControl('', [Validators.required, Validators.min(0)])
    });
  }

  ngOnInit(): void {
    this.getReview();
  }

  getReview(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.reviewService.getReview(id).toPromise().then(review => this.review = review);
  }

  save(): void {
    const id = this.review?.id;
    this.reviewService.updateReview(this.review)
      .toPromise().then(() => this.router.navigate(['/reviews/']));
  }

}
