import { Component, OnInit, Inject, Input } from '@angular/core';
import { Speaker } from '../model/speaker';
import { SpeakerService } from '../services/speakers/speaker.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-speaker-detail',
  templateUrl: './speaker-detail.component.html',
  styleUrls: ['./speaker-detail.component.css']
})

export class SpeakerDetailComponent implements OnInit {

  @Input() speaker?: Speaker;

  public speakerForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private speakerService: SpeakerService,
    private router: Router,
  ) {
    this.speakerForm = new FormGroup({
      first_name: new FormControl('', [Validators.required, Validators.min(0)]),
      last_name: new FormControl('', [Validators.required, Validators.min(0)]),
    });
  }

  ngOnInit(): void {
    this.getSpeaker();
  }

  getSpeaker(): void{
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.speakerService.getSpeaker(id)
      .toPromise().then(speaker => this.speaker = speaker);
  }

  save(): void {
    const id = this.speaker?.id;
    this.speakerService.updateSpeaker(this.speaker)
      .toPromise().then(() => this.router.navigate(['/speakers/']));
  }
}


