import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionsComponent } from './sessions/sessions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SessionDetailComponent } from './session-detail/session-detail.component';
import {SpeakersComponent} from './speakers/speakers.component';
import {SpeakerDetailComponent} from './speaker-detail/speaker-detail.component';
import {ReviewDetailComponent} from './review-detail/review-detail.component';
import {ReviewsComponent} from './reviews/reviews.component';

const routes: Routes = [
  { path: 'sessions', component: SessionsComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'speakers', component: SpeakersComponent},
  { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  { path: 'sessions/:id', component: SessionDetailComponent },
  { path: 'speakers/:id', component: SpeakerDetailComponent},
  { path: 'reviews/:id', component: ReviewDetailComponent },
  { path: 'reviews', component: ReviewsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
